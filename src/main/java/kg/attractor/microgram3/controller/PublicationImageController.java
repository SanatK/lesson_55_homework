package kg.attractor.microgram3.controller;

import kg.attractor.microgram3.dto.PublicationImageDTO;
import kg.attractor.microgram3.service.PublicationImageService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/images")
public class PublicationImageController {

    private final PublicationImageService publicationImageService;

    public PublicationImageController(PublicationImageService movieImageService) {
        this.publicationImageService = movieImageService;
    }

    @PostMapping
    public PublicationImageDTO addMovieImage(@RequestParam("file") MultipartFile file) {
        return publicationImageService.addImage(file);
    }

    @GetMapping("/{imageId}")
    public ResponseEntity<Resource> serveFile(@PathVariable String imageId) {
        Resource resource = publicationImageService.getById(imageId);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE)
                .body(resource);
    }
}
